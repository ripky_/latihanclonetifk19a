import 'package:first_flutter_project_ripky_19552011229/pages/edit_profile.dart';
import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: EditProfileState(),
    );
  }
}
