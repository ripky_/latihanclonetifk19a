import 'dart:html';
import 'package:first_flutter_project_ripky_19552011229/pages/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

class EditProfileState extends StatefulWidget {
  @override
  _EditProfileStateState createState() => _EditProfileStateState();
}

class _EditProfileStateState extends State<EditProfileState> {
  final globalKey = GlobalKey<ScaffoldState>();

  TextEditingController nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      body: Container(
        padding: EdgeInsets.only(
          top: 64,
          bottom: 64,
          left: 24,
          right: 24,
        ),
        child: (Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              child: Center(
                child: Text(
                  'EDIT PROFILE',
                  style: TextStyle(fontSize: 40, color: Colors.blueAccent),
                ),
              ),
            ),
            Container(
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Your Name'),
              ),
            ),
            Container(
              height: 48,
              child: FlatButton(
                color: Colors.blueAccent,
                child: Text(
                  'Update',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () {
                  if (nameController.text == '') {
                    showToast('Please input your name');
                    // showSnackBar('Please input your name');
                    // showAlertDialogMaterial('Please input your name');
                    // showAlertDialogCupertino('Please input your name');
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Profile(
                                name: nameController.text,
                              )),
                    );
                  }
                },
              ),
            ),
          ],
        )),
      ),
    );
  }

  showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black.withOpacity(0.5),
        textColor: Colors.white,
        fontSize: 16.0);
  }

  showSnackBar(text) {
    final snackbar = SnackBar(
      content: text(text),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {
          print('Clicked Undo!');
        },
      ),
    );
    // globalKey.currentState.showSnackBar(snackbar);
    Scaffold.of(context).showSnackBar(snackbar);

    showAlertDialogMaterial(text) {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text('Info'),
                content: Text('text'),
                actions: [
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      print('No, Dismiss');
                    },
                    child: Text('No'),
                  ),
                  FlatButton(
                    onPressed: () {
                      print('Yes, Accept');
                    },
                    child: Text('Yes'),
                  ),
                ],
              ));
    }

    showAlertDialogCupertino(text) {
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text('Info'),
                content: Text('text'),
                actions: [
                  CupertinoDialogAction(
                    onPressed: () {
                      Navigator.pop(context);
                      print('No, Dismiss');
                    },
                    child: Text('No'),
                  ),
                  CupertinoDialogAction(
                    onPressed: () {
                      print('Yes, Accept');
                    },
                    child: Text('Yes'),
                  ),
                ],
              ));
    }
  }
}
